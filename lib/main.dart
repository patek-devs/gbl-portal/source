import 'package:easy_localization/easy_localization.dart';
import 'package:firebase_core/firebase_core.dart';
import 'package:flutter/material.dart';

import 'gbl_portal.dart';

Future main() async {
  WidgetsFlutterBinding.ensureInitialized();
  await EasyLocalization.ensureInitialized();

  await Firebase.initializeApp();

  runApp(
    EasyLocalization(
      path: 'lang',
      supportedLocales: const <Locale>[
        Locale('en', 'US'),
        Locale('cs', 'CZ'),
      ],
      fallbackLocale: const Locale('en', 'US'),
      useOnlyLangCode: true,
      child: const GblPortal(),
    ),
  );
}
