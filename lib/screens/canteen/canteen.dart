import 'package:easy_localization/easy_localization.dart';
import 'package:flutter/material.dart';

import '/components/gbl_bar.dart';
import '/components/gbl_drawer.dart';
import '/services/canteen_service.dart';

class CanteenScreen extends StatefulWidget {
  const CanteenScreen({Key? key}) : super(key: key);

  @override
  State<CanteenScreen> createState() => _CanteenScreenState();
}

class _CanteenScreenState extends State<CanteenScreen> {
  late Future<List<Food>> futureMenu;

  @override
  void initState() {
    super.initState();
    futureMenu = CanteenService().fetchMenu();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: gblBar(context: context, title: 'canteen.title'.tr()),
      drawer: gblDrawer(context: context),
      body: FutureBuilder<List<Food>>(
        future: futureMenu,
        builder: (context, snapshot) {
          if (snapshot.hasData) {
            return SingleChildScrollView(
              child: Column(
                children: [
                  ListView.builder(
                    shrinkWrap: true,
                    primary: false,
                    itemCount: snapshot.data!.length,
                    itemBuilder: (context, index) {
                      return Card(
                        child: Column(
                          children: [
                            Text(
                              snapshot.data![index].dayName,
                              textAlign: TextAlign.left,
                              style: const TextStyle(
                                fontWeight: FontWeight.bold,
                                fontSize: 20,
                              ),
                            ),
                            Table(
                              border: const TableBorder(
                                horizontalInside: BorderSide(
                                  color: Colors.grey,
                                ),
                              ),
                              children: [
                                TableRow(
                                  children: [
                                    Text(
                                      'canteen.soup'.tr(),
                                      style: const TextStyle(
                                        fontWeight: FontWeight.bold,
                                      ),
                                    ),
                                    Text(snapshot.data![index].soup.name),
                                    Row(
                                      children: [
                                        for (var alergen in snapshot
                                            .data![index].soup.alergens)
                                          Text(
                                            '$alergen ',
                                            style: const TextStyle(
                                              fontSize: 8,
                                              color: Colors.grey,
                                            ),
                                          ),
                                      ],
                                    ),
                                  ],
                                ),
                                TableRow(
                                  children: [
                                    Text(
                                      'canteen.meal1'.tr(),
                                      style: const TextStyle(
                                        fontWeight: FontWeight.bold,
                                      ),
                                    ),
                                    Text(snapshot.data![index].meal1.name),
                                    Row(
                                      children: [
                                        for (var alergen in snapshot
                                            .data![index].meal1.alergens)
                                          Text(
                                            '$alergen ',
                                            style: const TextStyle(
                                              fontSize: 8,
                                              color: Colors.grey,
                                            ),
                                          ),
                                      ],
                                    ),
                                  ],
                                ),
                                if (snapshot.data![index].meal2 != null)
                                  TableRow(
                                    children: [
                                      Text(
                                        'canteen.meal2'.tr(),
                                        style: const TextStyle(
                                          fontWeight: FontWeight.bold,
                                        ),
                                      ),
                                      Text(snapshot.data![index].meal2!.name),
                                      Row(
                                        children: [
                                          for (var alergen in snapshot
                                              .data![index].meal2!.alergens)
                                            Text(
                                              '$alergen ',
                                              style: const TextStyle(
                                                fontSize: 8,
                                                color: Colors.grey,
                                              ),
                                            ),
                                        ],
                                      ),
                                    ],
                                  )
                              ],
                            )
                          ],
                        ),
                      );
                    },
                  ),
                ],
              ),
            );
          } else if (snapshot.hasError) {
            return Text(
              '${snapshot.error}',
            );
          }

          // By default, show a loading spinner.
          return const CircularProgressIndicator();
        },
      ),
    );
  }
}
