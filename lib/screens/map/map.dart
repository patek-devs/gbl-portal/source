import 'dart:io';

import 'package:easy_localization/easy_localization.dart';
import 'package:flutter/material.dart';
import 'package:flutter_svg/flutter_svg.dart';
import 'package:gbl_portal/services/storage_service.dart';
import 'package:path_provider/path_provider.dart';
import 'package:shared_preferences/shared_preferences.dart';

import '/components/gbl_bar.dart';
import '/components/gbl_drawer.dart';

enum Floor {
  basement,
  ground,
  first,
  second,
}

enum PlanType {
  shortcodes,
  names,
  numbers,
  newNumbers,
}

class MapScreen extends StatefulWidget {
  const MapScreen({Key? key}) : super(key: key);

  @override
  State<MapScreen> createState() => _MapScreenState();
}

class _MapScreenState extends State<MapScreen> {
  late Floor _floor;
  late PlanType _type;
  late Future<bool> _loaded;
  late Directory assetDir;

  @override
  void initState() {
    super.initState();
    _loaded = _getPrefs();
  }

  Future<bool> _getPrefs() async {
    bool _finished = false;
    await SharedPreferences.getInstance().then((SharedPreferences sp) {
      final String _typeStr =
          'PlanType.${sp.getString('mapDefaultType') ?? 'newNumbers'}';
      _type = PlanType.values
          .firstWhere((element) => element.toString() == _typeStr);

      final String _floorStr =
          'Floor.${sp.getString('mapDefaultFloor') ?? 'ground'}';
      _floor =
          Floor.values.firstWhere((element) => element.toString() == _floorStr);

      setState(() {});

      _finished = true;
    });

    if (_finished) {
      _finished = await StorageService().fetchMaps();
      assetDir = await getApplicationDocumentsDirectory();
    }

    return _finished;
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: gblBar(context: context, title: 'plan.title'.tr()),
      drawer: gblDrawer(context: context),
      body: FutureBuilder(
        future: _loaded,
        builder: (context, snapshot) {
          if (snapshot.hasData) {
            if (snapshot.data == true) {
              return _getMaps();
            } else {
              return const Text('Loading preferences failed horribly');
            }
          } else {
            return const CircularProgressIndicator();
          }
        },
      ),
    );
  }

  Widget _getMaps() {
    return Column(
      children: [
        Row(
          mainAxisAlignment: MainAxisAlignment.spaceEvenly,
          children: [
            Text('${'plan.floor.label'.tr()}: '),
            DropdownButton(
              value: _floor.toString(),
              onChanged: (newValue) {
                final Floor _fl = Floor.values.firstWhere(
                  (element) => element.toString() == newValue.toString(),
                );
                setState(() {
                  _floor = _fl;
                });
              },
              items: Floor.values.map((Floor floor) {
                return DropdownMenuItem<String>(
                  value: floor.toString(),
                  child:
                      Text('plan.floor.${floor.toString().split('.')[1]}').tr(),
                );
              }).toList(),
            ),
            Text('${'plan.type.label'.tr()}: '),
            DropdownButton(
              value: _type.toString(),
              onChanged: (newValue) {
                final PlanType _pT = PlanType.values.firstWhere(
                  (element) => element.toString() == newValue.toString(),
                );
                setState(() {
                  _type = _pT;
                });
              },
              items: PlanType.values.map((PlanType type) {
                return DropdownMenuItem<String>(
                  value: type.toString(),
                  child:
                      Text('plan.type.${type.toString().split('.')[1]}').tr(),
                );
              }).toList(),
            ),
          ],
        ),
        Expanded(
          child: InteractiveViewer(
            minScale: 1,
            maxScale: 10,
            child: Center(
              child: _getPlan(context: context, floor: _floor, type: _type),
            ),
          ),
        ),
      ],
    );
  }

  Widget _getPlan({
    required BuildContext context,
    required Floor floor,
    required PlanType type,
  }) {
    final String _asset =
        '${assetDir.path}/maps/${floor.toString().split('.')[1]}/${type.toString().split('.')[1]}.svg';
    String _svg = File(_asset).readAsStringSync();
    _svg = _svg.replaceAll(
      '<?xml version="1.0" encoding="UTF-8" standalone="no"?>\n<!-- Created with Inkscape (http://www.inkscape.org/) -->\n\n',
      "",
    );

    return SvgPicture.string(
      _svg,
      width: MediaQuery.of(context).size.width * 0.9,
    );
  }
}
