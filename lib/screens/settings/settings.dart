import 'package:card_settings/card_settings.dart';
import 'package:easy_localization/easy_localization.dart';
import 'package:flutter/material.dart';
import 'package:shared_preferences/shared_preferences.dart';

import '/components/gbl_bar.dart';
import '/components/gbl_drawer.dart';
import '../map/map.dart';

class SettingsScreen extends StatefulWidget {
  const SettingsScreen({Key? key}) : super(key: key);

  @override
  State<SettingsScreen> createState() => _SettingsScreenState();
}

class _SettingsScreenState extends State<SettingsScreen> {
  final _keyForm = GlobalKey<FormState>();
  late SharedPreferences _preferences;
  Widget _body = const CircularProgressIndicator();
  late List<String> _floorList;
  late List<String> _planTypeList;
  late String _floor;
  late String _planType;

  Future<void> _getPrefs() async {
    SharedPreferences.getInstance().then((prefs) {
      _preferences = prefs;
      _getOptions();
    });
  }

  void _getOptions({Locale? locale}) async {
    List<String> _flL;
    List<String> _pTL;
    String _fl;
    String _pT;

    if (locale != null) {
      await context.setLocale(locale);
    }
    _flL = Floor.values
        .map((value) => tr('plan.floor.${value.toString().split('.')[1]}'))
        .toList();
    _pTL = PlanType.values
        .map((value) => tr('plan.type.${value.toString().split('.')[1]}'))
        .toList();
    final String _flString = tr(
      'plan.floor.${_preferences.getString('mapDefaultFLoor') ?? 'ground'}',
    );
    _fl = _flL.firstWhere((element) => element == _flString);
    final String _pTString = tr(
      'plan.type.${_preferences.getString('mapDefaultType') ?? 'newNumbers'}',
    );
    _pT = _pTL.firstWhere((element) => element == _pTString);

    setState(() {
      _floorList = _flL;
      _planTypeList = _pTL;
      _floor = _fl;
      _planType = _pT;
    });

    final _bd = _buildSettingsList();

    setState(() {
      _body = _bd;
    });

    print('options reloaded');
  }

  @override
  void initState() {
    super.initState();

    _getPrefs();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: gblBar(title: 'Settings', context: context),
      drawer: gblDrawer(context: context),
      body: _body,
    );
  }

  Widget _buildSettingsList() {
    return Form(
      key: _keyForm,
      child: CardSettings(
        children: <CardSettingsSection>[
          CardSettingsSection(
            header: CardSettingsHeader(
              label: 'settings.general.label'.tr(),
            ),
            children: [
              CardSettingsListPicker(
                label: 'settings.general.language'.tr(),
                items: context.supportedLocales
                    .map(
                      (locale) => locale.toStringWithSeparator(separator: ' '),
                    )
                    .toList(),
                initialItem:
                    context.locale.toStringWithSeparator(separator: ' '),
                onChanged: (String? newLocale) {
                  _getOptions(locale: newLocale!.toLocale(separator: ' '));
                },
              ),
            ],
          ),
          CardSettingsSection(
            header: CardSettingsHeader(
              label: 'settings.map.label'.tr(),
            ),
            children: [
              CardSettingsListPicker(
                key: Key(_floor),
                label: 'settings.map.floor'.tr(),
                items: _floorList,
                initialItem: _floor,
                onChanged: (String? value) {
                  final floor = tr('plan.floor.${value!}');
                  _preferences.setString('mapDefaultFloor', floor);
                },
              ),
              CardSettingsListPicker(
                key: Key(_planType),
                label: 'settings.map.type'.tr(),
                items: _planTypeList,
                initialItem: _planType,
                onChanged: (String? value) {
                  _preferences.setString(
                    'mapDefaultType',
                    tr('plan.type.${value!}'),
                  );
                },
              )
            ],
          ),
        ],
      ),
    );
  }
}
