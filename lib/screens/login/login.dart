import 'package:flutter/material.dart';
import 'package:gbl_portal/components/user_managment/signin_button.dart';
import 'package:vrouter/vrouter.dart';

import '/components/gbl_bar.dart';
import '/components/gbl_drawer.dart';

class LoginScreen extends StatefulWidget {
  const LoginScreen({Key? key}) : super(key: key);

  @override
  State<LoginScreen> createState() => _LoginScreenState();
}

class _LoginScreenState extends State<LoginScreen> {
  void signedIn(dynamic newUser, BuildContext context) {
    if (newUser != null) {
      context.vRouter.toNamed('Map');
    }
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: gblBar(title: 'login', context: context),
      drawer: gblDrawer(context: context),
      body: SafeArea(
        child: Padding(
          padding: const EdgeInsets.only(left: 16.0, right: 16.0, bottom: 20.0),
          child: Column(
            children: [
              Row(),
              Expanded(
                child: Column(
                  mainAxisSize: MainAxisSize.min,
                  mainAxisAlignment: MainAxisAlignment.center,
                  children: [
                    const Text('GBL Portal'),
                    SignInButton(signedIn),
                  ],
                ),
              ),
            ],
          ),
        ),
      ),
    );
  }
}
