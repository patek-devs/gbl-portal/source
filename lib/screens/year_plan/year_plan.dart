import 'package:flutter/material.dart';
import 'package:intl/intl.dart';

import '/components/gbl_bar.dart';
import '/components/gbl_drawer.dart';
import '/services/plan_service.dart';

class YearPlanScreen extends StatefulWidget {
  const YearPlanScreen({Key? key}) : super(key: key);

  @override
  State<YearPlanScreen> createState() => _YearPlanScreenState();
}

class _YearPlanScreenState extends State<YearPlanScreen> {
  late Future<List<Event>> events;

  @override
  void initState() {
    super.initState();
    events = PlanService().fetchPlan();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: gblBar(title: 'School year plan', context: context),
      drawer: gblDrawer(context: context),
      body: FutureBuilder<List<Event>>(
        future: events,
        builder: (context, snapshot) {
          if (snapshot.hasData) {
            return SingleChildScrollView(
              child: Column(
                children: [
                  ListView.builder(
                    shrinkWrap: true,
                    itemCount: snapshot.data!.length,
                    itemBuilder: (context, index) {
                      return Card(
                        child: Column(
                          children: [
                            Text(
                              snapshot.data![index].title,
                              style: const TextStyle(
                                fontWeight: FontWeight.bold,
                                fontSize: 15,
                              ),
                            ),
                            Row(
                              children: [
                                Text(
                                  DateFormat('dd. MM. yyyy')
                                      .format(snapshot.data![index].start),
                                ),
                                Expanded(
                                  child: Align(
                                    alignment: Alignment.centerRight,
                                    child: Text(
                                      DateFormat('dd. MM. yyyy')
                                          .format(snapshot.data![index].end),
                                    ),
                                  ),
                                ),
                              ],
                            )
                          ],
                        ),
                      );
                    },
                  )
                ],
              ),
            );
          } else if (snapshot.hasError) {
            return Text('${snapshot.error}');
          }
          return const CircularProgressIndicator();
        },
      ),
    );
  }
}
