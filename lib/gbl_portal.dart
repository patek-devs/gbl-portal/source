import 'package:easy_localization/easy_localization.dart';
import 'package:flutter/material.dart';
import 'package:vrouter/vrouter.dart';

import 'screens/calendar/calendar.dart';
import 'screens/canteen/canteen.dart';
import 'screens/login/login.dart';
import 'screens/map/map.dart';
import 'screens/settings/settings.dart';
import 'services/authentication_service.dart';
import 'theme/gbl_theme.dart';

class GblPortal extends StatelessWidget {
  const GblPortal({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return VRouter(
      debugShowCheckedModeBanner: false,
      // langs
      localizationsDelegates: context.localizationDelegates,
      supportedLocales: context.supportedLocales,
      locale: context.locale,
      // routes
      mode: VRouterMode.history,
      initialUrl: '/calendar',
      routes: [
        VGuard(
          beforeEnter: (VRedirector) async =>
              !AuthenticationService.hasPermissions()
                  ? VRedirector.to('/login')
                  : null,
          stackedRoutes: [
            VWidget(
              path: '/map',
              widget: _pathWidgetBuilder(child: const MapScreen()),
              name: 'Map',
            ),
          ],
        ),
        VWidget(
          path: '/settings',
          widget: _pathWidgetBuilder(child: const SettingsScreen()),
          name: 'Settings',
        ),
        VWidget(
          path: '/login',
          widget: _pathWidgetBuilder(child: const LoginScreen()),
          name: 'Login',
        ),
        VWidget(
          path: '/canteen',
          widget: _pathWidgetBuilder(child: const CanteenScreen()),
          name: 'Canteen',
        ),
        VWidget(
          path: '/calendar',
          widget: _pathWidgetBuilder(child: const CalendarScreen()),
          name: 'Calendar',
        ),
      ],
    );
  }

  Widget _pathWidgetBuilder({required Widget child}) {
    return Theme(
      data: gblTheme,
      child: _wrapBanner(child: child),
    );
  }

  Widget _wrapBanner({required Widget child}) {
    return Banner(
      message: "BETA",
      location: BannerLocation.topEnd,
      color: Colors.green,
      child: child,
    );
  }
}
