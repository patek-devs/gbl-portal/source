import 'package:easy_localization/easy_localization.dart';
import 'package:flutter/material.dart';
import 'package:vrouter/vrouter.dart';

import 'user_managment/user_manager.dart';

Widget gblDrawer({required BuildContext context}) {
  return Drawer(
    child: ListView(
      padding: EdgeInsets.zero,
      children: [
        const SizedBox(
          height: 220.0,
          child: DrawerHeader(
            child: UserManager(),
          ),
        ),
        ListTile(
          title: const Text('drawer.map').tr(),
          onTap: () {
            context.vRouter.toNamed('Map');
          },
        ),
        ListTile(
          title: const Text('drawer.canteen').tr(),
          onTap: () {
            context.vRouter.toNamed('Canteen');
          },
        ),
        ListTile(
          title: const Text('drawer.calendar').tr(),
          onTap: () {
            context.vRouter.toNamed('Calendar');
          },
        ),
        ListTile(
          title: const Text('drawer.settings').tr(),
          onTap: () {
            context.vRouter.toNamed('Settings');
          },
        ),
      ],
    ),
  );
}
