import 'package:easy_localization/easy_localization.dart';
import 'package:firebase_auth/firebase_auth.dart';
import 'package:flutter/material.dart';
import 'package:vrouter/vrouter.dart';

import '/services/authentication_service.dart';
import 'signin_button.dart';

class UserManager extends StatefulWidget {
  const UserManager({Key? key}) : super(key: key);

  @override
  State<UserManager> createState() => _UserManagerState();
}

class _UserManagerState extends State<UserManager> {
  User? _user;

  @override
  void initState() {
    _user = FirebaseAuth.instance.currentUser;

    super.initState();
  }

  refresh(User? newUser, context) {
    if (newUser != null) {
      setState(() {
        _user = newUser;
      });
    } else {
      setState(() {
        _user = FirebaseAuth.instance.currentUser;
      });
    }
  }

  @override
  Widget build(BuildContext context) {
    if (_user == null) {
      return SignInButton(refresh);
    } else {
      return Column(
        crossAxisAlignment: CrossAxisAlignment.end,
        children: <Widget>[
          Row(
            children: <Widget>[
              Image.network(
                _user?.photoURL ?? 'https://via.placeholder.com/100C',
                width: 100.0,
                fit: BoxFit.fitHeight,
              ),
              Text(
                _user?.displayName ?? 'Unknown User',
                style: const TextStyle(fontSize: 15),
              ),
            ],
          ),
          OutlinedButton(
            onPressed: () async {
              await AuthenticationService.signOut(context: context);

              refresh(null, null);

              if (['/map', '/timetables'].contains(VRouter.of(context).path)) {
                context.vRouter.toNamed('Login');
              }
            },
            child: Text('user.signOut'.tr()),
          ),
        ],
      );
    }
  }
}
