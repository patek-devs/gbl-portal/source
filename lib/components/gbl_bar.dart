import 'package:flutter/material.dart';

PreferredSizeWidget gblBar({
  required String title,
  required BuildContext context,
}) {
  return AppBar(
    title: Text(title),
  );
}
