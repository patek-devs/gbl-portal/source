import 'package:flutter/material.dart';

MaterialColor gblMaterialColor = const MaterialColor(0xFF1473B7, {
  50: Color(0xFFE3EEF6),
  100: Color(0xFFB9D5E9),
  200: Color(0xFF8AB9DB),
  300: Color(0xFF5B9DCD),
  400: Color(0xFF3788C2),
  500: Color(0xFF1473B7),
  600: Color(0xFF126BB0),
  700: Color(0xFF0E60A7),
  800: Color(0xFF0B569F),
  900: Color(0xFF064390)
});

ThemeData gblTheme = ThemeData(
  brightness: Brightness.light,
  visualDensity: VisualDensity.comfortable,
  primarySwatch: gblMaterialColor,
);
