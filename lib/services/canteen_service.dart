import 'package:html/dom.dart';
import 'package:html/parser.dart';
import 'package:http/http.dart' as http;

class CanteenService {
  Future<List<Food>> fetchMenu() async {
    final response = await http.get(
      Uri.parse(
        'https://us-central1-gbl-site.cloudfunctions.net/canteenCorser',
      ),
    );

    if (response.statusCode == 200) {
      return _parseMenu(response.body);
    } else {
      throw Exception('Failed to load menu');
    }
  }

  List<Food> _parseMenu(String page) {
    final Document menu = parse(page, encoding: 'UTF-8');

    final days = menu.getElementsByClassName('objednavka-obalka');

    final List<Food> result = [];

    for (final day in days) {
      final String dayName =
          day.getElementsByClassName('objednavka-den-datum')[0].text.trim();
      final meals = day.getElementsByClassName('objednavka-jidlo-obalka');
      if (meals.length >= 3) {
        final Meal? soup = getMeal(meals[0]);
        final Meal? meal1 = getMeal(meals[1]);
        if (meal1 != null) {
          final Meal? meal2 = getMeal(meals[2]);

          final Food food = Food(
            dayName: dayName,
            soup: soup!,
            meal1: meal1,
            meal2: meal2,
          );

          result.add(food);
        }
      }
    }

    return result;
  }

  Meal? getMeal(Element element) {
    final name =
        element.getElementsByClassName('objednavka-jidlo-nazev')[0].text.trim();

    if (name.isEmpty ||
        name == "přesnídáv." ||
        name == 'oběd 1' ||
        name == 'oběd 2') {
      return null;
    }

    final alergenElement =
        element.getElementsByClassName('objednavka-jidlo-alergeny-udaje');

    final List<String> alergens = [];

    if (alergenElement.isNotEmpty) {
      final alergenList = alergenElement[0].text.split('-');

      for (final alergen in alergenList) {
        final text = alergen.trim();

        if (text.isNotEmpty) {
          if (text[0] == '0' || text[0] == '1') {
            alergens.add(text);
          } else if (text[text.length - 2] == '0' ||
              text[text.length - 2] == '1') {
            alergens.add(text.substring(text.length - 2));
          }
        }
      }
    }

    return Meal(
      name: name,
      alergens: alergens,
    );
  }
}

class Food {
  final String dayName;
  final Meal soup;
  final Meal meal1;
  final Meal? meal2;

  Food({
    required this.dayName,
    required this.soup,
    required this.meal1,
    required this.meal2,
  });
}

class Meal {
  final String name;
  final List<String> alergens;

  Meal({
    required this.name,
    required this.alergens,
  });
}
