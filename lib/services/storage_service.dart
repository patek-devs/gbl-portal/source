import 'dart:convert';
import 'dart:io';

import 'package:archive/archive.dart';
import 'package:firebase_storage/firebase_storage.dart' as firebase_storage;
import 'package:path_provider/path_provider.dart';

class StorageService {
  late Directory directory;
  late File metaFile;
  late Map<String, dynamic> metaMap;

  Future<bool> fetchMaps() async {
    final firebase_storage.FirebaseStorage storage =
        firebase_storage.FirebaseStorage.instance;

    final String currentTag = await getCacheMeta();

    final firebase_storage.ListResult list = await storage.ref().listAll();

    for (final firebase_storage.Reference ref in list.items) {
      final firebase_storage.FullMetadata meta = await ref.getMetadata();
      if (meta.name == "plans.zip") {
        final String? custom =
            meta.customMetadata?['customMetadata']?.replaceAll("'", '"');
        final Map<String, dynamic> customDecoded =
            jsonDecode(custom!) as Map<String, dynamic>;
        final String? tag = customDecoded.cast<String, String>()["tag"];

        if (tag != currentTag) {
          final File zip = File('${directory.path}/plans.zip');
          try {
            await ref.writeToFile(zip);
          } catch (e) {
            print(e);
            return false;
          }

          final zipBytes = await zip.readAsBytes();

          final Archive archive = ZipDecoder().decodeBytes(zipBytes);

          for (final ArchiveFile file in archive) {
            final String filename = file.name;
            if (file.isFile) {
              final data = file.content as List<int>;
              File('${directory.path}/maps/$filename')
                ..createSync(recursive: true)
                ..writeAsBytesSync(data);
            } else {
              Directory('${directory.path}/maps/$filename')
                  .create(recursive: true);
            }
          }
          metaMap["tag"] = tag;
          metaFile.writeAsString(jsonEncode(metaMap));

          // zip.delete();
        }
        print(Directory(directory.path).listSync(recursive: true));
        return true;
      }
    }
    return false;
  }

  Future<String> getCacheMeta() async {
    directory = await getApplicationDocumentsDirectory();
    final path = '${directory.path}/metadata.json';
    metaFile = await File(path).create(recursive: true);

    String data = await metaFile.readAsString();
    if (data.isEmpty) data = "{}";
    metaMap = jsonDecode(data) as Map<String, dynamic>;
    return metaMap.cast<String, String>()["tag"] ?? "";
  }
}
