import 'package:html/parser.dart';
import 'package:http/http.dart' as http;

class PlanService {
  Map<String, int> months = {
    "ledna": 1,
    "února": 2,
    "března": 3,
    "dubna": 4,
    "května": 5,
    "června": 6,
    "července": 7,
    "srpna": 8,
    "září": 9,
    "října": 10,
    "listopadu": 11,
    "prosince": 12
  };

  Future<List<Event>> fetchPlan() async {
    final response = await http.get(
      Uri.parse('https://gbl.cz/pr%C3%A1zdniny/'),
      headers: {"Access-Control-Allow-Origin": "*"},
    );

    if (response.statusCode == 200) {
      return _parsePlan(response.body);
    } else {
      throw Exception('Failed to load plan');
    }
  }

  List<Event> _parsePlan(String page) {
    final plan = parse(page, encoding: 'UTF-8');

    final events = plan
        .getElementsByClassName('content')[0]
        .getElementsByTagName('tbody')[0]
        .getElementsByTagName('tr');

    final List<Event> result = [];

    for (final event in events) {
      final time = event.getElementsByTagName('td')[0].text.trim().split(' ');
      final title = event.getElementsByTagName('td')[1].text.trim();

      final dayStart = int.parse(time[0].replaceAll('.', ''));
      final monthStart = months[time[1]] ?? 0;
      final yearStart = int.parse(time[2]);

      var dayEnd = dayStart;
      var monthEnd = monthStart;
      var yearEnd = yearStart;
      if (time.length > 4) {
        dayEnd = int.parse(time[5].replaceAll('.', ''));
        monthEnd = months[time[6]] ?? 0;
        yearEnd = int.parse(time[7]);
      }

      final DateTime start = DateTime.utc(yearStart, monthStart, dayStart);
      final DateTime end = DateTime.utc(yearEnd, monthEnd, dayEnd);

      final Event res = Event(start: start, end: end, title: title);

      result.add(res);
    }

    return result;
  }
}

class Event {
  final DateTime start;
  final DateTime end;
  final String title;

  Event({required this.start, required this.end, required this.title});
}
