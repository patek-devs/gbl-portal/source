#!/usr/bin/env bash

GREEN='\033[0;32m'
RED='\033[0;31m'
CYAN='\033[1;36m'
RESET='\033[0;0m'

tag=""
pubspec=""
app_ver=""

i=1
j=$#
while [ $i -le $j ]
do
    case $1 in
        -f)
            pubspec="$2"
        ;;
        -t)
            tag="$2"
        ;;
    esac
    
    i=$((i + 1))
    
    shift 1
done

tag=${tag#v}

printf "${CYAN}Testing tag (${tag})${RESET}\n"
if ! [[ "$tag" =~ ^[[:digit:]]+\.[[:digit:]]+\.[[:digit:]]+(\-[a-zA-Z0-9.]+)?(\+[a-zA-Z0-9.]+)?$ ]]
then
    printf "${RED}Tag is invalid!${RESET}\nRefer to https://semver.org/ for reference...\n"
    exit 1
fi

printf "${GREEN}Tag is valid!${RESET}\n"


printf "${CYAN}Reading app version${RESET}\n"

while read -r LINE; do
    if [[ $LINE =~ version:\ [[:digit:]]+\.[[:digit:]]+\.[[:digit:]]+.* ]]
    then
        app_ver=${LINE#*\ }
    fi
done < $pubspec

if [[ "$app_ver" != "" ]]
then
    printf "${GREEN}App version is: ${app_ver}${RESET}\n"
else
    printf "${RED}App version in $pubspec not found!${RESET}\n"
    exit 1
fi


printf "${CYAN}Comparing tag with app version${RESET}\n"

tag_col=""
app_col=""
same=1

# disecting tag
tag_main=${tag%\+*}
tag_main=${tag_main%\-*}
tag_build=${tag#$tag_main}

tag_major=${tag_main#*\ }
tag_major=${tag_major%%\.*}

tag_minor=${tag_main#*\.}
tag_minor=${tag_minor%\.*}

tag_patch=${tag_main##*\.}
tag_patch=${tag_patch%%[+-]*}

# disecting app_ver
app_main=${app_ver%\+*}
app_main=${app_main%\-*}
app_build=${app_ver#$app_main}

app_major=${app_main#*\ }
app_major=${app_major%%\.*}

app_minor=${app_main#*\.}
app_minor=${app_minor%\.*}

app_patch=${app_main##*\.}
app_patch=${app_patch%%[+-]*}

# comparing
if (( "$app_major" == "$tag_major" ))
then
    tag_col="${tag_col}${tag_major}"
    app_col="${app_col}${app_major}"
else
    tag_col="${tag_col}${RED}${tag_major}${RESET}"
    app_col="${app_col}${RED}${app_major}${RESET}"
    same=0
fi

if (( "$app_minor" == "$tag_minor" ))
then
    tag_col="${tag_col}.${tag_minor}"
    app_col="${app_col}.${app_minor}"
else
    tag_col="${tag_col}.${RED}${tag_minor}${RESET}"
    app_col="${app_col}.${RED}${app_minor}${RESET}"
    same=0
fi

if (( "$app_patch" == "$tag_patch" ))
then
    tag_col="${tag_col}.${tag_patch}"
    app_col="${app_col}.${app_patch}"
else
    tag_col="${tag_col}.${RED}${tag_patch}${RESET}"
    app_col="${app_col}.${RED}${app_patch}${RESET}"
    same=0
fi

if [[ "$tag_build" =~ .*\-.* ]] && [[ "$app_build" =~ .*\-.* ]]
then
    tag_pre=${tag_build%+*}
    tag_pre=${tag_pre#-}
    app_pre=${app_build%+*}
    app_pre=${app_pre#-}
    
    if (( "$tag_pre" == "$app_pre" ))
    then
        tag_col="${tag_col}-${tag_pre}"
        app_col="${app_col}-${app_pre}"
    else
        tag_col="${tag_col}-${RED}${tag_pre}${RESET}"
        app_col="${app_col}-${RED}${app_pre}${RESET}"
        same=0
    fi
elif [[ "$tag_build" =~ .*\-.* ]]
then
    tag_pre=${tag_build%+*}
    tag_pre=${tag_pre#-}
    
    tag_col="${tag_col}-${RED}${tag_pre}${RESET}"
    same=0
elif [[ "$app_build" =~ .*\-.* ]]
then
    app_pre=${app_build%+*}
    app_pre=${app_pre#-}
    
    app_col="${app_col}-${RED}${app_pre}${RESET}"
    same=0
fi

if [[ "$tag_build" =~ .*\+.* ]] && [[ "$app_build" =~ .*\+.* ]]
then
    tag_post=${tag_build#*+}
    app_post=${app_build#*+}
    
    if (( "$tag_post" == "$app_post" ))
    then
        tag_col="${tag_col}+${tag_post}"
        app_col="${app_col}+${app_post}"
    else
        tag_col="${tag_col}+${RED}${tag_post}${RESET}"
        app_col="${app_col}+${RED}${app_post}${RESET}"
        same=0
    fi
elif [[ "$tag_build" =~ .*\+.* ]]
then
    tag_post=${tag_build#*+}
    
    tag_col="${tag_col}+${RED}${tag_post}${RESET}"
    same=0
elif [[ "$app_build" =~ .*\+.* ]]
then
    app_post=${app_build#*+}
    app_col="${app_col}+${RED}${app_post}${RESET}"
    same=0
fi

if (( $same ))
then
    printf "${GREEN}Tag and App version are the same!${RESET}\n"
else
    printf "${RED}Tag and App version are not the same!${RESET}\ntag: ${tag_col}\napp: ${app_col}\n"
    exit 1
fi

exit 0
