#!/usr/bin/env bash

ORANGE='\033[0;33m'
GREEN='\033[0;32m'
NC='\033[0m'

file_path=""
major=false
minor=false
patch=false
build=false
beta=false
verbose=false

i=1
j=$#
while [ $i -le $j ]
do
    case $1 in
        -f)
            file_path="$2"
        ;;
        -v)
            verbose=true
        ;;
        --major)
            major=true
        ;;
        --minor)
            minor=true
        ;;
        --patch)
            patch=true
        ;;
        --build)
            build=true
        ;;
        --beta)
            beta=true
        ;;
    esac
    
    i=$((i + 1))
    
    shift 1
done

if [[ $major == true ]]; then minor=false; patch=false; build=false
    elif [[ $minor == true ]]; then patch=false; build=false;
elif [[ $patch == true ]]; then build=false; fi

if [[ $verbose == true ]]; then
    printf "${ORANGE}version control starting${NC}\n"
    printf "file: $file_path\n"
    printf "major: $major | minor:$minor | patch:$patch | build: $build | beta: $beta\n"
fi

while read -r LINE; do
    if [[ $LINE =~ version:\ [[:digit:]]+\.[[:digit:]]+\.[[:digit:]]+.* ]]; then
        major_value=${LINE#*\ }
        major_value=${major_value%%\.*}
        
        minor_value=${LINE#*\.}
        minor_value=${minor_value%\.*}
        
        patch_value=${LINE##*\.}
        patch_value=${patch_value%%[+-]*}
        
        build_value=${LINE#*[+-]}
        
        if [[ $build_value =~ beta* ]]; then
            build_value=-$build_value
        else
            build_value=+$build_value
        fi
        
        break;
    fi
done < $file_path


if [[ $verbose == true ]]; then printf "old version: $major_value.$minor_value.$patch_value$build_value\n"; fi

if [[ $major == true ]];
then
    major_value=$(($major_value + 1));
    minor_value=0;
    patch_value=0;
    build_value='';
fi

if [[ $minor == true ]];
then
    minor_value=$(($minor_value + 1));
    patch_value=0;
    build_value='';
fi

if [[ $patch == true ]];
then
    patch_value=$(($patch_value + 1));
    build_value='';
    
fi

if [[ $build == true ]]; then
    build_number=$((${build_value##*\+}+1))
    build_value=${build_value%\+*}+$build_number
fi

if [[ $build_value =~ -beta* ]] && [[ $beta == false ]];
then
    build_value=${build_value#\-beta};
elif [[ ! $build_value =~ -beta ]];
then
    build_value="-beta$build_value";
fi

if [[ $verbose == true ]]; then printf "new version: $major_value.$minor_value.$patch_value$build_value\n"; fi
sed -i --regexp-extended -e "s/version: [[:digit:]]+\.[[:digit:]]+\.[[:digit:]]+.*/version: $major_value.$minor_value.$patch_value$build_value/g" $file_path


if [[ $verbose == true ]]; then printf "${GREEN}version control finished${NC}\n";
else echo "$major_value.$minor_value.$patch_value$build_value"; fi
