import base64
import json
import os
import re
import sys
from platform import architecture
from uuid import uuid4

import firebase_admin
from firebase_admin import credentials, storage


def get_paths():
    i = 3

    paths = {"android": [], "ios": [], "web": []}
    while i < len(sys.argv):
        directory = sys.argv[i]
        for root, directories, files in os.walk(directory):
            for filename in files:
                # join the two strings in order to form the full filepath.
                filepath = os.path.join(root, filename)
                if root.endswith("android"):
                    paths["android"].append(filepath)
                elif root.endswith("ios"):
                    paths["ios"].append(filepath)
                elif root.endswith("web"):
                    paths["web"].append(filepath)
        i += 1

    return(paths)


def upload(bucket, file: str, tag: str, platform: str):
    print("Uploading " + file)
    cloud_path = "release/"+platform+"/"
    path = file.split('/')
    file_extension = path[-1].split('.')[-1]
    # splits android architectures
    if platform == "android":
        if file_extension != 'aab':
            for string in path:
                if string.endswith('.apk'):
                    architecture = string
                    architecture = architecture.removesuffix('release.apk')
                    architecture = architecture.removeprefix('app')
                    architecture = architecture.strip('-')
                    if architecture != '':
                        cloud_path += architecture+"/"
                    else:
                        cloud_path += "universal/"
        else:
            cloud_path += "bundle/"

    cloud_path += tag + "." + file_extension
    blob = bucket.blob(cloud_path)

    accessToken = uuid4()

    metadata = {"firebaseStorageDownloadTokens": accessToken}

    blob.metadata = metadata

    blob.upload_from_filename(file)

    blob.make_public()


def main():
    paths = get_paths()
    key64 = sys.argv[1]
    tag = sys.argv[2]

    keyJSON = base64.b64decode(key64)

    key = json.loads(keyJSON)

    cred = credentials.Certificate(key)
    firebase_admin.initialize_app(cred, {
        'storageBucket': 'gblportal-d0655.appspot.com'
    })

    bucket = storage.bucket()
    for platform in paths.keys():
        for file in paths[platform]:
            upload(bucket, file, tag, platform)


if __name__ == "__main__":
    main()
