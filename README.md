[![GitLab Release (latest by SemVer)](https://img.shields.io/gitlab/v/release/patek-devs/gbl-portal/source?style=flat)](https://gitlab.com/patek-devs/gbl-portal/source/-/releases/latest)
[![Platforms](https://img.shields.io/badge/platform-android-lightgrey?style=flat)]()
[![Android version](https://img.shields.io/badge/Android-%5E4.4-blue?style=flat)](https://developer.android.com/about/versions/kitkat)

[![style: lint](https://img.shields.io/badge/style-lint-4BC0F5.svg)](https://pub.dev/packages/lint)
[![pipeline status](https://gitlab.com/patek-devs/gbl-portal/source/badges/main/pipeline.svg)](https://gitlab.com/patek-devs/gbl-portal/source/-/commits/main)

# GBL Portal

An all in one app for students of Gymnázium J. S. Machara...

## Features

Well nothing much at the moment :cry:

## Roadmap

- iOS support
- working map of school
- canteen menu
- school year plan
- current events
- timetables
- Google classroom integration

---

Created by [Tomáš Kysela](https://tkysela.cz) from [Pátek](https://patekvpatek.cz/)
